class Box < ApplicationRecord
  validates :name, presence: true
  validates :shipping_date, presence: true
  validates :shipping_time, presence: true

  validates :colors, length: { minimum:1, maximum: 2 }

  validates :material, presence: true


  belongs_to :material
  has_and_belongs_to_many :colors
end
