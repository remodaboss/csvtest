class AddMaterialIdToBox < ActiveRecord::Migration[5.2]
  def change
    add_column :boxes, :material_id, :int
  end
end
