class CreateBoxes < ActiveRecord::Migration[5.2]
  def change
    create_table :boxes do |t|
      t.string :name
      t.integer :price
      t.integer :weight

      t.timestamps
    end
  end
end
