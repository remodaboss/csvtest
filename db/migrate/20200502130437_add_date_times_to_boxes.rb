class AddDateTimesToBoxes < ActiveRecord::Migration[5.2]
  def change
    add_column :boxes, :shipping_date, :date
    add_column :boxes, :shipping_time, :time
  end
end
