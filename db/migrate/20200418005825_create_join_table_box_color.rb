class CreateJoinTableBoxColor < ActiveRecord::Migration[5.2]
  def change
    create_join_table :boxes, :colors do |t|
      # t.index [:box_id, :color_id]
      # t.index [:color_id, :box_id]
    end
  end
end
