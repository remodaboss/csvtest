# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#Material.create(id:1,name: 'Wood')
#Material.create(id:2,name: 'Plastic')
#Material.create(id:3,name: 'Glass')

Color.create(id:1, name: 'Red')
Color.create(id:2, name: 'Green')
Color.create(id:3, name: 'Blue')
